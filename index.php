<?php

	require 'vendor/autoload.php';

	use GuzzleHttp\Client;

	$app = new \Slim\Slim(array(
		'templates.path' => './templates',
		'lms_base_url' => 'http://lms.leisurepoolsdigital.com',
		'lms_client_id' => 'NTU0ODQwNTdiNTg0OGYy',
		'lms_api_secret' => 'e274e810c6f67290289c7d9e358d5ddd843936d7',
		'api_access_token' => '/oauth/token',
		'grant_type' => 'client_credentials',
		'api_lead_submit_endpoint' => '/leads/create.json'
	));
	
	// get the default homepage and render the layout.
	$app->get('/', function () use ($app) {
		// grab the home page.
	    $app->render('home.php');
	});

	$app->get('/get_token', function() use ($app) {

		$url = $app->config('lms_base_url') . $app->config('api_access_token');
		$client_id = $app->config('lms_client_id');
		$grant_type = $app->config('grant_type');
		$client_secret = $app->config('lms_api_secret');

		$client = new Client();
		$response = $client->post($url, 
			array(
				'body' => array(
					'client_id' => $client_id,
					'client_secret' => $client_secret,
					'grant_type' => $grant_type
		)));

        echo $response->getBody();
	});

	$app->post('/submit_quote', function() use ($app) {
		$response = $app->request->post();

		echo json_encode($response);
	});

	$app->run();