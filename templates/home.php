<!DOCTYPE HTML>
<html>
	<head>
		<title>Leisure Pools Perth</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta property="og:title" content= "Fibreglass Swimming Pools in Perth" /> 
		<meta property="og:type" content="page" /> 
		<meta property="og:description" content="For information about a Fibreglass swimming pool and a free quote, see Leisure Pools today!" /> 
		<meta property="og:url" content= "http://www.leisurepools.com.au.com.au" />
		<meta property="og:image" content= "http://www.leisurepools.com.au/wp-content/uploads/2015/02/PoolTips_2.jpg" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="../assets/js/jquery.min.js"></script>
		<script src="../assets/js/jquery.scrolly.min.js"></script>
		<script src="../assets/js/skel.min.js"></script>
		<script src="../assets/js/init.js"></script>
		<script src="../assets/js/client.js"></script>
		<noscript>
			<link rel="stylesheet" href="../assets/css/skel.css" />
			<link rel="stylesheet" href="../assets/css/style.css" />
			<link rel="stylesheet" href="../assets/css/style-xlarge.css" />
		</noscript>
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body>

		<!-- Header -->
			<section id="header">
				<video autoplay id="bgvid" muted loop>
				<source src="../assets/video/Fiberglass-pools.webm" type="video/webm">
				</video>

				<div class="overlay"></div>

				<div class="inner">
					<div class="logo"><img src="../assets/images/Logo.svg" alt="Leisure Pools" /></div>
					<h1>Cutting-edge <strong>Fibreglass Swimming Pools</strong>,<br /> available in Perth</h1>
					<p>Our local dealers are the experts who can help you get setup and on your way to having your own in-ground fibreglass pool installed in your own backyard.</p>

					<ul class="actions">
						<li><a href="#one" class="button scrolly">Free Quote</a></li>
					</ul>
				</div>
			</section>

		<!-- One -->
			<section id="one" class="main style1">
				<div class="container">
					<div class="row 150%">
						<div class="6u 12u$(medium)">
							<header class="major">
								<h2>Get a <strong>free quote</strong> now<br />
								just fill in the form here.</h2>
							</header>
							<p>We have a highly experienced team of dealers to guide you through the process. Once you fill out the form, one of our staff will contact you within 24 hours and you'll be well on your way to achieving your dream and starting your lifestyle of <strong>luxury.</strong></p>
						</div>
						<div class="6u$ 12u$(medium) important(medium)">
							<h2 class="special">Get A <strong>Free Quote</strong> Here</h2>
							<p>Includes a <strong>free</strong> Book Brochure!</p>
							<form id="quote-submit" action="#">
								<div class="form-group">
									<label for="fname">First Name: </label><input type="text" name="fname" id="fname" placeholder="your first name" class="form-control"><br />
									<label for="lname">Last Name: </label><input type="text" name="lname" id="lname" placeholder="your family name" class="form-control"><br />
									<label for="email">Email Address: </label><input type="email" name="email" id="email" placeholder="your email" class="form-control"><br />
									<label for="postcode">Postcode: </label><input type="text" name="postcode" id="postcode" placeholder="your post code" class="form-control"><br />
									<input type="submit" class="button special fit" id="submit-btn" value="Free Quote"/>
								</div>
							</form>
						</div>
					</div>
				</div>
			</section>

		<!-- Call Section -->
			<section id="call">
				<div class="main">
					<header class="major special">
						<p>Or call us on</p>
						<h2>
							<a href="tel:+1300775274">
								<span itemprop="telephone">1300 775 274</span>
							</a>
						</h2>
					</header>
				</div>
			</section>		

			<!-- Pools Section -->
			<section id="information" style="background-image:url('../assets/images/Fiberglass_pools/processed/platinum-plunge-Fiberglass-pool.jpg');background-size: cover; background-repeat: no-repeat;" class="main style2">
				<div class="overlay"></div>
				<div class="container">					
					<header class="major special">
						<h2>If you are looking for a swimming pool...</h2>
					</header>
					<div class="row">
						<div class="6u 12u$(medium)">
							<strong>Our National Pool Dealer Network</strong>
							<p>To view our full range of products including equipment for your pool and beautiful water features, head over to our company website.</p>
							<p>
								<a href="http://www.leisurepools.com.au/fibreglass-swimming-pools" alt="Fibreglass Swimming Pool Showroom" class="button" target="_blank" title="Fibreglass Swimming Pool Showroom">Pool Showroom</a> &nbsp; 
								<a href="http://www.leisurepoolsonline.com/" target="_blank" alt="Fibreglass Swimming Pool Showroom" class="button" title="Fibreglass Swimming Pool Showroom">Global Site</a></p>
							<hr />										
						</div>
						<div class="6u$ 12u$(medium)">
							<div itemscope itemtype="http://schema.org/LocalBusiness">
							   <span itemprop="name"><strong>Leisure Pools Perth</strong></span>
							   <address itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
							     <span itemprop="streetAddress">55 Winton Road</span><br />
							     <span itemprop="addressLocality">Joondalup</span>,
							     <span itemprop="addressRegion">WA</span>
							     <span itemprop="postalCode">6027</span><br />
							   </address>
							   <strong>Phone:</strong> <span itemprop="telephone">1300 775 274</span>
							</div>
						</div>
					</div>
				</div>
			</section>

		<!-- Footer -->
			<section id="footer">
				<ul class="icons">
					<li><a href="https://twitter.com/leisurepools" class="icon alt fa-twitter"><span class="label">Twitter</span></a></li>
					<li><a href="https://www.facebook.com/pages/Leisure-Pools/393209294079587" class="icon alt fa-facebook"><span class="label">Facebook</span></a></li>
				</ul>
			</section>
	</body>
</html>
