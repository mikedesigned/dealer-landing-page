/*
	# This script is intended to communicate through internal routes within this application.
	#
	# It's grabs the Token from our external CMS API and then uses that token to submit a lead
	# right down into LMS without having to do anything! Yay! :)
*/


function getAccessToken(){
	var token = $.ajax({
		type: "GET",
		url: "/get_token",
		dataType: "json",
		success: function(data){
			console.log("Got the token.");
			token = data.access_token;
		}
	});

	return token;
}

function submitLead(formData) {
	var lead = $.ajax({
		type: "POST",
		url: "/submit_quote",
		dataType: "json",
		data: formData,
		success: function(data){
			console.log(data);
		}
	});
}

$(document).ready(function(){
	$("#quote-submit").submit(function(e){
		e.preventDefault();
		$(this).fadeOut().parent().append("<h2>Working...</h2>");

		var accessToken = getAccessToken();
		console.log("Token" + accessToken);
		data = $(this).serialize();

		console.log(data);
		submitLead();
	});
});